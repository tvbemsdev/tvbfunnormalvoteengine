angular.module("myapp", []).controller("mycontroller", function($scope, $http) {

	$scope.optionIds = "";
	$scope.programName = "";
	$scope.programTime = "";
	$scope.ans_option = "allans";

	var self = this;
	var getresult = "";
	self.school = [ {} ];
	$scope.totalunique = 0;
	document.getElementById("status_row").style.display = "none";
	document.getElementById("submit_old").disabled = true;
	document.getElementById("submit_new").disabled = true;
	self.submitStatistics = function() {
		console.log($scope.programIds);
		var username = getCookie("username");

		if (username == "" || username == undefined || username == null) {
			alert("Please Login Again!");
			window.location = "./login.html";
			return;
		}

		if ($scope.programIds == "" || $scope.programIds == undefined) {
			alert("Missing Program Id");
			return;
		}

		var urlParam = "?";
		urlParam += "&program_ids=" + $scope.programIds.replace(" ", "").trim();

		var urlPath = window.location.origin + "/luckydraw" + "/ResultStatisticServlet" + encodeURI(urlParam);

		var request = {
			method : "POST",
			url : urlPath,
			crossDomain : true,
			contentType : "application/json"
		};

		var callbackSuccess = function(response) {
			console.log(urlPath);
			console.log(response);
			getresult = "1";
			_.each(response.data, function(program) {

				var choice_s = [];
				$scope.totalunique = 0;

				_.each(program.choice, function(choiceP, i) {

					choice_s.push(choiceP);
					$scope.totalunique += choiceP.count;
					console.log(choiceP);

					choiceP.display = String.fromCharCode(65 + i);
				});

				self.school = choice_s;

			});

			$scope.listStatistics = response.data;
			console.log(self.school);

		};

		var callbackError = function(response) {
			console.log(response);
		};

		$http(request).success(callbackSuccess).error(callbackError);

	};
	document.getElementById('condition_ans_table').style.display = "none";
	self.checkAnsOption = function() {

	}

	self.submitluckyDraw = function() {
		if ($scope.programIds == "" || $scope.programIds == undefined) {
			alert("Missing Program Ids");
			console.log("noproid");
			return;
		}

		if ($scope.luckydrawNum == "" || $scope.luckydrawNum == undefined) {
			alert("Missing Lucky Draw Number");
			return;
		}

		if ($scope.luckydrawNum.replace(" ", "").trim() > 2000) {
			alert("Maximun 2000");
			return;
		}
		
		if ($scope.optionIds != "") {
			if (getresult == "" || getresult == undefined) {
				alert("Please Click Get Result Statistics Button");
				return;
			}
		}

		var mapping = "";
		var urlParam = "";
		var urlPath = "";
		var curlPath = "";

		if ($scope.programName == "" || $scope.programName == undefined) {
			alert("Missing Program Name");
			return;
		}
		if ($scope.programTime == "" || $scope.programTime == undefined) {
			alert("Missing Program Time");
			return;
		}
		if ($scope.optionIds != "") {
			_.each($scope.listStatistics[0].choice, function(choiceP, i) {

				if (mapping != "") {
					mapping += ";";
				}
				mapping += choiceP.choice + ":" + choiceP.display;
			});
		}
		var program_ids = $scope.programIds.replace(" ", "").trim();
		var program_name = $scope.programName.trim();
		var program_time = $scope.programTime.trim();
		var option_ids = $scope.optionIds.replace(" ", "").trim();
		var luckydraw_num = $scope.luckydrawNum.replace(" ", "").trim();
		var mapping = mapping.replace(" ", "").trim();
		if (!$scope.quiz_ans == "")
			var quiz_ans = $scope.quiz_ans.replace(" ", "").trim();
		else
			var quiz_ans = "".replace(" ", "").trim();
		urlPath = window.location.origin + "/luckydraw" + "/LuckyDrawServlet" + encodeURI(urlParam);

		// Request

		var callbackSuccess = function(response) {
			if (response.resultcode == 200) {
				alert("LuckyDraw Success");
				document.getElementById("submit_old").disabled = false;
				document.getElementById("submit_new").disabled = false;
			} else {
				alert("LuckyDraw Fail");
			}
		};

		var callbackError = function(response) {
			alert("LuckyDraw Fail");
		};

		var requestInfo = {
			method : "POST",
			url : window.location.origin + "/luckydraw/LuckyDrawServlet",
			data : JSON.stringify({
				'program_ids' : program_ids,
				'option_ids' : option_ids,
				'luckydraw_num' : luckydraw_num,
				'quiz_ans' : quiz_ans,
				'mapping' : mapping

			}),
			crossDomain : true,
			contentType : "application/json",
		};

		$http(requestInfo).success(callbackSuccess).error(callbackError);

	};
	self.submit_old = function() {

		var mapping = "";
		var urlParam = "";
		var urlPath = "";

		if ($scope.optionIds != "") {
			_.each($scope.listStatistics[0].choice, function(choiceP, i) {

				if (mapping != "") {
					mapping += ";";
				}
				mapping += choiceP.choice + ":" + choiceP.display;
			});
		}
		urlParam = "?&program_ids=" + $scope.programIds.replace(" ", "").trim();

		urlPath = window.location.origin + "/luckydraw" + "/LuckyDrawOldFormatServlet" + encodeURI(urlParam);

		window.location.assign(urlPath);

	};
	self.submit_new = function() {

		var mapping = "";
		var urlParam = "";
		var urlPath = "";
		if ($scope.optionIds != "") {
			_.each($scope.listStatistics[0].choice, function(choiceP, i) {

				if (mapping != "") {
					mapping += ";";
				}
				mapping += choiceP.choice + ":" + choiceP.display;
			});
		}
		urlParam = "?&program_ids=" + $scope.programIds.replace(" ", "").trim();
		urlParam += "&program_name=" + $scope.programName.trim();
		urlParam += "&program_time=" + $scope.programTime.trim();

		urlPath = window.location.origin + "/luckydraw" + "/LuckyDrawNewFormatServlet" + encodeURI(urlParam);

		window.location.assign(urlPath);

	};

	function getCookie(name) {
		var regexp = new RegExp("(?:^" + name + "|;\s*" + name + ")=(.*?)(?:;|$)", "g");
		var result = regexp.exec(document.cookie);
		return (result === null) ? null : result[1];
	}

});