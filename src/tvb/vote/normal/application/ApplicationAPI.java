package tvb.vote.normal.application;

import javax.ws.rs.ApplicationPath;
import org.glassfish.jersey.server.ResourceConfig;
 
@ApplicationPath("api")
public class ApplicationAPI extends ResourceConfig{
    public ApplicationAPI(){
        packages("tvb.vote.normal.servlet");
    }
}
