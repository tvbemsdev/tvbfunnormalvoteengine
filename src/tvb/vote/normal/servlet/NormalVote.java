package tvb.vote.normal.servlet;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Context;

import org.json.JSONArray;
import org.json.JSONObject;

import com.mysql.cj.jdbc.MysqlDataSource;

import tvb.vote.normal.listener.StartUpListener;

@Path("/normal_vote")
public class NormalVote {
	
	private final MysqlDataSource mysqlDS;
	private final String strNormalVoteTable;
	
	@Context HttpServletRequest request;
	@Context HttpServletResponse response;
	@Context ServletContext context;

	public NormalVote() {
		mysqlDS = StartUpListener.mysqlDS;
		strNormalVoteTable = StartUpListener.strDBTableName;
	}

	@POST
	@Path("/{program_id}/vote")
	public String submitVote(@PathParam("program_id") String strProgramId, String strBody) {
		
		int intResultCode = 5;
		
		System.out.println("**********************************\n"
				+ "**** Body   : " + strBody);
		
		if (!strBody.equals("")){
			
			JSONObject jsonInput = null;
			try {
				jsonInput = new JSONObject(strBody);
			} catch (Exception e){
				jsonInput = null;
			}
			
			if (jsonInput != null){
				String strMemberId   = jsonInput.getString("appmemberid");
				String strSessionKey = jsonInput.getString("sessionkey");
				String strMemberType = jsonInput.getString("membertype");
				String strPlatform   = jsonInput.getString("platform");
				String strInfo       = jsonInput.getString("info");
				
				String strOptionId   = "";
				JSONArray jsonOption = jsonInput.getJSONArray("optionid");
				for (int i=0; i<jsonOption.length(); i++){
					if (!strOptionId.equals("")) strOptionId += ",";
					strOptionId += jsonOption.get(i);
				}
				
				Connection conn = null;
				PreparedStatement stmt = null;
				ResultSet rs = null;
				
				try{
					conn = mysqlDS.getConnection();
					//conn.setAutoCommit(false);
		
					String strSQL = "SELECT COUNT(*) FROM " + strNormalVoteTable + " where program_id = ? and member_id = ?";
					stmt = conn.prepareStatement(strSQL);
					stmt.setString(1, strProgramId);
					stmt.setString(2, strMemberId);
					
					boolean bolInsert = true;
					
					rs = stmt.executeQuery();
					rs.next();
					
					//System.out.println("SELECT : " + rs.getInt(1));
					if (rs.getInt(1) > 0){
						bolInsert = false;
					}
					rs.close();
					stmt.close();
					
					if (bolInsert){
						strSQL = "INSERT INTO " + strNormalVoteTable + " (program_id, member_id, sessionkey, member_type, platform, info, option_ids, last_updated_date, created_date) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";
					} else {
						strSQL = "UPDATE " + strNormalVoteTable + " SET program_id=?, member_id=?, sessionkey=?, member_type=?, platform=?, info=?, option_ids=?, last_updated_date=? where program_id = ? and member_id = ?";
					}
					
					System.out.println("**********************************\n"
							+ "**** MemberId   : " + strMemberId
							+ "\n** SessionKey : " + strSessionKey
							+ "\n**** MemberType : " + strMemberType
							+ "\n**** Platform   : " + strPlatform
							+ "\n**** Info       : " + strInfo
							+ "\n**** OptionId   : " + strOptionId
							+ "\n**** Time       : " + new java.util.Date()
							+ "\n**** SQL        : " + strSQL);
					
					stmt = conn.prepareStatement(strSQL);
					stmt.setString(1, strProgramId);
					stmt.setString(2, strMemberId);
					stmt.setString(3, strSessionKey);
					stmt.setString(4, strMemberType);
					stmt.setString(5, strPlatform);
					stmt.setString(6, strInfo);
					stmt.setString(7, strOptionId);
					stmt.setTimestamp(8, new Timestamp(new java.util.Date().getTime()));
					
					if (bolInsert){
						stmt.setTimestamp(9, new Timestamp(new java.util.Date().getTime()));
					} else {
						stmt.setString(9, strProgramId);
						stmt.setString(10, strMemberId);
					}
		
					stmt.executeUpdate();
					//conn.commit();
					stmt.close();
					conn.close();
					
					intResultCode = 0;
					
				} catch (SQLException e) {
					//try {
					//	conn.rollback();
					//} catch (SQLException e1) {
					//	e1.printStackTrace();
					//}
					e.printStackTrace();
					
				} finally {
					try {
						if (rs != null)
							rs.close();
						if (stmt != null)
							stmt.close();
						if (conn != null)
							conn.close();
					} catch (SQLException e) {
						e.printStackTrace();
					}
				}
			}
		}
		
		JSONObject jsonReturn = new JSONObject();
		jsonReturn.put("resultcode", intResultCode);
		
		return jsonReturn.toString();
	}
	
	@POST
	@Path("/{program_id}/seriousvote")
	public String submitSeriousVote(@PathParam("program_id") String strProgramId, String strBody) {
		return this.submitVote(strProgramId, strBody);
	}
}
