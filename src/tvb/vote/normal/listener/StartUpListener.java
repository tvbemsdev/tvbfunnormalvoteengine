package tvb.vote.normal.listener;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Properties;
import java.util.concurrent.ConcurrentHashMap;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.mysql.cj.jdbc.MysqlDataSource;

import tvb.vote.normal.model.LuckyDrawResult;
import tvb.vote.normal.model.MemberInfo;
import tvb.vote.normal.thread.UpdateMemberThread;

public class StartUpListener implements ServletContextListener{

	private String strPropFile = "../system.properties";
	
	public static ArrayList<String[]> arrMemberDBInfo = null;
	public static ConcurrentHashMap<String, MemberInfo> mapMemberInfo = null;
	public static MysqlDataSource mysqlDS = null;
	
	public static String strDBDriver = "";
	public static String strDBURL = "";
	public static String strDBUser = "";
	public static String strDBPassword = "";
	public static String strDBTableName = "";
	
	public static MysqlDataSource mysqlDS_LuckyDraw = null;
	public static String strDBDriver_LuckyDraw = "";
	public static String strDBURL_LuckyDraw = "";
	public static String strDBUser_LuckyDraw = "";
	public static String strDBPassword_LuckyDraw = "";
	public static String strDBTableName_LuckyDraw = "";
	
	public static int intMaxInactiveInterval = 30 * 60;
	
	public static ArrayList<LuckyDrawResult> LuckyDrawList;
	
	public StartUpListener(){
		
		System.out.println("**** StartUpListener [INIT]");
		
		arrMemberDBInfo = new ArrayList<String[]>();
		mapMemberInfo = new ConcurrentHashMap<String, MemberInfo>();
		mysqlDS = new MysqlDataSource();
		mysqlDS_LuckyDraw = new MysqlDataSource();
	}

	@Override
	public void contextDestroyed(ServletContextEvent arg0) {
		// TODO Auto-generated method stub
		System.out.println("destroyed");
	}

	@Override
	public void contextInitialized(ServletContextEvent arg0) {
		// TODO Auto-generated method stub
		
		this.getMemberDBInfo();
		this.getNormalVoteDBInfo();
		
		mysqlDS.setURL(strDBURL);
		mysqlDS.setUser(strDBUser);
		mysqlDS.setPassword(strDBPassword);
		mysqlDS_LuckyDraw.setURL(strDBURL_LuckyDraw);
		mysqlDS_LuckyDraw.setUser(strDBUser_LuckyDraw);
		mysqlDS_LuckyDraw.setPassword(strDBPassword_LuckyDraw);
		
		UpdateMemberThread threadMember = new UpdateMemberThread();
		System.out.println("thread is alive : " + threadMember.isAlive());
//		threadMember.start();
	}
	
	private void getMemberDBInfo(){
		
		System.out.println("********************************************************************");
		System.out.println("**** StartUpListener - getMemberDBInfo");
		
		try {
			
			ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
			
			Properties properties = new Properties();
//	        properties.load(configCPServlet.getServletContext().getResourceAsStream(strPropFile));
//			properties.load(new FileInputStream(strPropFile));
			
			properties.load(classLoader.getResourceAsStream(strPropFile));
	        
	        String strNoOfMemberDB = properties.getProperty("no_of_member_db");
	        System.out.println("Property value is strNoOfMemberDB: " + strNoOfMemberDB);
	        
	        int intNoOfMemberDB = 0;
	        try {
	        	intNoOfMemberDB = Integer.parseInt(strNoOfMemberDB);
	        } catch (Exception e){
	        	intNoOfMemberDB = 0;
	        }
	        System.out.println("Property value is intNoOfMemberDB: " + intNoOfMemberDB);
	        
	        
	        if (intNoOfMemberDB > 0){
	        	
	        	for (int i=1; i<=intNoOfMemberDB; i++){
	        		
	        		System.out.println("********************************************************************");
	        		
	        		String[] strMemberInfo = new String[4];
	        		
	        		String strPropDBDriver = properties.getProperty("member_db_" + i + "_driver");
	    	        System.out.println("Property value is strPropDBDriver[" + i + "]: " + strPropDBDriver);
	    	        
	    	        if (strPropDBDriver != null && !strPropDBDriver.equals("") && !strPropDBDriver.equals("null")){
	    	        	strMemberInfo[0] = strPropDBDriver;
	    	        	System.out.println("Property value is strMemberInfo[" + i + " - 0]: " + strMemberInfo[0]);	
	    	        }
	    	        
	    	        String strPropDBURL = properties.getProperty("member_db_" + i + "_url");
	    	        System.out.println("Property value is strPropDBURL[" + i + "]: " + strPropDBURL);
	    	        
	    	        if (strPropDBURL != null && !strPropDBURL.equals("") && !strPropDBURL.equals("null")){
	    	        	strMemberInfo[1] = strPropDBURL;
	    	        	System.out.println("Property value is strMemberInfo[" + i + " - 1]: " + strMemberInfo[1]);	
	    	        }
	    	        
	    	        String strPropDBUser = properties.getProperty("member_db_" + i + "_user");
	    	        System.out.println("Property value is strPropDBUser[" + i + "]: " + strPropDBUser);
	    	        
	    	        if (strPropDBUser != null && !strPropDBUser.equals("") && !strPropDBUser.equals("null")){
	    	        	strMemberInfo[2] = strPropDBUser;
	    	        	System.out.println("Property value is strMemberInfo[" + i + " - 2]: " + strMemberInfo[2]);	
	    	        }
	    	        
	    	        String strPropDBPassword = properties.getProperty("member_db_" + i + "_password");
	    	        System.out.println("Property value is strPropDBPassword[" + i + "]: " + strPropDBPassword);
	    	        
	    	        if (strPropDBPassword != null && !strPropDBPassword.equals("") && !strPropDBPassword.equals("null")){
	    	        	strMemberInfo[3] = strPropDBPassword;
	    	        	System.out.println("Property value is strMemberInfo[" + i + " - 3]: " + strMemberInfo[3]);	
	    	        }
	        		
	    	        arrMemberDBInfo.add(strMemberInfo);
	        	}
	        }
	        
		} catch (Exception e){
			e.printStackTrace();
		}
	}
	
	private void getNormalVoteDBInfo(){
		
		System.out.println("********************************************************************");
		System.out.println("**** StartUpListener - getRewardDBInfo");
		
		try {
			
			ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
			
			Properties properties = new Properties();
//	        properties.load(configCPServlet.getServletContext().getResourceAsStream(strPropFile));
//			properties.load(new FileInputStream(strPropFile));
			
			properties.load(classLoader.getResourceAsStream(strPropFile));
	     
	        String strPropDBDriver = properties.getProperty("db_driver");
	        String strPropLuckyDrawDBDriver = properties.getProperty("db_luckydraw_driver");
	        System.out.println("Property value is strPropDBDriver: " + strPropDBDriver);
	        
	        if (strPropDBDriver != null && !strPropDBDriver.equals("") && !strPropDBDriver.equals("null")){
	        	strDBDriver = strPropDBDriver;
	        	strDBDriver_LuckyDraw=strPropLuckyDrawDBDriver;
	        	System.out.println("Property value is strDBDriver: " + strDBDriver);	
	        }
	        
	        String strPropDBURL = properties.getProperty("db_url");
	        String strPropLuckyDrawDBURL = properties.getProperty("db_luckydraw_url");
	        System.out.println("Property value is strPropDBURL: " + strPropDBURL);
	        
	        if (strPropDBURL != null && !strPropDBURL.equals("") && !strPropDBURL.equals("null")){
	        	strDBURL = strPropDBURL;
	        	strDBURL_LuckyDraw=strPropLuckyDrawDBURL;
	        	System.out.println("Property value is strDBURL: " + strDBURL);	
	        }
	        
	        String strPropDBUser = properties.getProperty("db_user");
	        String strPropLuckyDrawDBUser = properties.getProperty("db_luckydraw_user");
	        System.out.println("Property value is strPropDBUser: " + strPropDBUser);
	        
	        if (strPropDBUser != null && !strPropDBUser.equals("") && !strPropDBUser.equals("null")){
	        	strDBUser = strPropDBUser;
	        	strDBUser_LuckyDraw=strPropLuckyDrawDBUser;
	        	System.out.println("Property value is strDBUser: " + strDBUser);	
	        }
	        
	        String strPropDBPassword = properties.getProperty("db_password");
	        String strPropLuckyDrawDBPassword = properties.getProperty("db_luckydraw_password");
	        System.out.println("Property value is strPropDBPassword: " + strPropDBPassword);
	        
	        if (strPropDBPassword != null && !strPropDBPassword.equals("") && !strPropDBPassword.equals("null")){
	        	strDBPassword = strPropDBPassword;
	        	strDBPassword_LuckyDraw=strPropLuckyDrawDBPassword;
	        	System.out.println("Property value is strDBPassword: " + strDBPassword);	
	        }
	        
	        String strPropTableName = properties.getProperty("db_table_name");
	        String strPropLuckyDrawTableName = properties.getProperty("db_luckydraw_table_name");
	        System.out.println("Property value is strPropTableName: " + strPropTableName);
	        
	        if (strPropTableName != null && !strPropTableName.equals("") && !strPropTableName.equals("null")){
	        	strDBTableName = strPropTableName;
	        	strDBTableName_LuckyDraw=strPropLuckyDrawTableName;
	        	System.out.println("Property value is strDBTableName: " + strDBTableName);	
	        }
	        
		} catch (Exception e){
			e.printStackTrace();
		}
	}
	
	public static boolean isLogined(HttpServletRequest request){
		HttpSession session =  request.getSession(false);
		return (session != null);
	}
	
	public static String returnErrorMessage(Exception e){
		Writer wr = new StringWriter();
    	PrintWriter pwr = new PrintWriter(wr);
    	e.printStackTrace(pwr);
    	return wr.toString();
	}
}
