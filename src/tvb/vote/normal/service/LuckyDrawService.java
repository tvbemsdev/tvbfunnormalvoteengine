package tvb.vote.normal.service;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Random;
import java.util.Collections;
import java.util.HashMap;

import tvb.vote.normal.listener.StartUpListener;
import tvb.vote.normal.model.LuckyDrawResult;

public class LuckyDrawService {

	private SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:SS");

	public void doLuckyDraw(String strProgramID, String strOptionID, String strLuckyDrawNum, String strMapping, String strQuizAns) {
		ArrayList<String> quiz_memberArraylist = null;

		if (strOptionID.equals("")) {
			getVoteLuckyDraw(strProgramID, strLuckyDrawNum, strQuizAns);
		} else {
			// Get Full Member List with Correct Quiz Answer
			if (!strQuizAns.equals("") && !strQuizAns.equals("null") && strQuizAns != null) {
				quiz_memberArraylist = getQuiz_memberArraylist(strProgramID, strQuizAns);
				System.out.println("Quiz member list size" + quiz_memberArraylist.size());
			}

			// Get Full Member List with Correct Answer
			ArrayList<LuckyDrawResult> arrLuckyDrawResult = getLuckyDrawResultList(strProgramID, strOptionID, strMapping, strQuizAns);

			// Copy the List to Public
			Collections.shuffle(arrLuckyDrawResult, new Random(3));

			int intLuckyDrawNum = Integer.parseInt(strLuckyDrawNum);
			int a = 0;

			if (null == quiz_memberArraylist) {
				while (a < arrLuckyDrawResult.size() && StartUpListener.LuckyDrawList.size() < intLuckyDrawNum) {
					StartUpListener.LuckyDrawList.add(arrLuckyDrawResult.get(a));
					a++;
				}

			} else {
				// Filter out all Member With wrong Quiz Answer
				while (a < arrLuckyDrawResult.size() && StartUpListener.LuckyDrawList.size() < intLuckyDrawNum) {
					for (int i = 0; i < quiz_memberArraylist.size(); i++) {
						String strMemberID_Quiz = quiz_memberArraylist.get(i).toUpperCase();
						String strMemberID_LuckyDraw = arrLuckyDrawResult.get(a).getMember_id().toUpperCase();

						if (strMemberID_Quiz.equals(strMemberID_LuckyDraw)) {
							StartUpListener.LuckyDrawList.add(arrLuckyDrawResult.get(a));
						}
					}
					a++;
				}
			}
		}

		// Find the Member Mobile Number
		searchMemberPhone();

	}

	private ArrayList<String> getQuiz_memberArraylist(String strProgramID, String strQuizAns) {
		ArrayList<String> quiz_memberArraylist = new ArrayList<String>();

		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;

		String strSQL = "";
		if (!strQuizAns.equals("") && !strQuizAns.equals("null") && strQuizAns != null) {
			try {
				strSQL = "select member_id from luckydraw where program_id = '" + strProgramID + "' and quiz_answer = '" + strQuizAns + "'";
				System.out.println("**** SQL : " + strSQL);

				conn = DriverManager.getConnection(StartUpListener.strDBURL_LuckyDraw, StartUpListener.strDBUser_LuckyDraw, StartUpListener.strDBPassword_LuckyDraw);
				stmt = conn.createStatement();
				rs = stmt.executeQuery(strSQL);

				while (rs.next()) {
					quiz_memberArraylist.add(rs.getString("member_id"));
				}

				rs.close();
				stmt.close();
				conn.close();

			} catch (Exception e) {
				e.printStackTrace();

			} finally {
				try {
					if (rs != null)
						rs.close();
					if (stmt != null)
						stmt.close();
					if (conn != null)
						conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}

		return quiz_memberArraylist;
	}

	private ArrayList<LuckyDrawResult> getVoteLuckyDraw(String strProgramID, String strLuckyDrawNum, String strQuizAns) {

		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;

		String strSQL = "";
		if (!strQuizAns.equals("") && !strQuizAns.equals("null") && strQuizAns != null) {
			try {
				
				int size = Integer.parseInt(strLuckyDrawNum);
				
				strSQL = "select member_id, update_time from luckydraw where program_id = '" + strProgramID + "' and quiz_answer = '" + strQuizAns + "' order by rand() limit " + size;
				System.out.println("**** SQL : " + strSQL);

				conn = DriverManager.getConnection(StartUpListener.strDBURL_LuckyDraw, StartUpListener.strDBUser_LuckyDraw, StartUpListener.strDBPassword_LuckyDraw);
				stmt = conn.createStatement();
				rs = stmt.executeQuery(strSQL);		

				while (rs.next()) {
					
					LuckyDrawResult luckyDrawResult = new LuckyDrawResult();

					luckyDrawResult.setUpdate_time(sdf.format(new java.util.Date(rs.getTimestamp("update_time").getTime())));
					luckyDrawResult.setMember_id(rs.getString("member_id").replace("-", "").toUpperCase());
					luckyDrawResult.setChoice_id("");
					luckyDrawResult.setChoice("");
					luckyDrawResult.setScore("");

					StartUpListener.LuckyDrawList.add(luckyDrawResult);		
					
				}
				
				System.out.println("xxx " + StartUpListener.LuckyDrawList.size());

				rs.close();
				stmt.close();
				conn.close();

			} catch (Exception e) {
				e.printStackTrace();

			} finally {
				try {
					if (rs != null)
						rs.close();
					if (stmt != null)
						stmt.close();
					if (conn != null)
						conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}

		return StartUpListener.LuckyDrawList;
	}

	private ArrayList<LuckyDrawResult> getLuckyDrawResultList(String strProgramID, String strOptionID, String strMapping, String strQuizAns) {
		ArrayList<LuckyDrawResult> arrLuckyDrawResult = new ArrayList<LuckyDrawResult>();

		HashMap<String, String> mapChoice = new HashMap<String, String>();
		String[] strChoices = strMapping.split(";");
		for (int i = 0; i < strChoices.length; i++) {
			String[] strChoice = strChoices[i].split(":");
			mapChoice.put(strChoice[0], strChoice[1]);
		}

		//////
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;

		String strSQL = "";

		if (!strOptionID.equals("") && !strOptionID.equals("null") && strOptionID != null) {
			String strAnswerQuery = "";

			if (strOptionID.indexOf(",") != -1) {
				String[] strAnswer = strOptionID.split(",");
				for (int i = 0; i < strAnswer.length; i++) {
					if (!strAnswerQuery.equals("")) {
						strAnswerQuery += " OR ";
					}
					strAnswerQuery += "option_ids = '" + strAnswer[i].trim() + "'";
				}
				strAnswerQuery = "(" + strAnswerQuery.trim() + ")";
			} else {
				strAnswerQuery = "option_ids = '" + strOptionID.trim() + "'";
			}

			strSQL = "SELECT member_id, last_updated_date, option_ids FROM tvb_fun_normal_vote where program_id='" + strProgramID + "' AND " + strAnswerQuery + " ORDER BY RAND();";

		} else {
			strSQL = "SELECT member_id, last_updated_date, option_ids FROM tvb_fun_normal_vote where program_id='" + strProgramID + "'ORDER BY RAND();";
		}

		try {

			System.out.println("**** SQL : " + strSQL);

			Class.forName(StartUpListener.strDBDriver);

			conn = DriverManager.getConnection(StartUpListener.strDBURL, StartUpListener.strDBUser, StartUpListener.strDBPassword);
			stmt = conn.createStatement();
			rs = stmt.executeQuery(strSQL);

			String strAnswerFlag = "1";
			if (strOptionID == null || strOptionID.equals("")) {
				strAnswerFlag = "0";
			}

			while (rs.next()) {
				LuckyDrawResult luckyDrawResult = new LuckyDrawResult();
				String strOption_ids = rs.getString("option_ids");
				String strOption = mapChoice.get(strOption_ids);
				if (null == strOption) {
					strOption = "";
				}

				luckyDrawResult.setUpdate_time(sdf.format(new java.util.Date(rs.getTimestamp("last_updated_date").getTime())));
				luckyDrawResult.setMember_id(rs.getString("member_id").replace("-", "").toUpperCase());
				luckyDrawResult.setChoice_id(strOption_ids);
				luckyDrawResult.setChoice(strOption);
				luckyDrawResult.setScore(strAnswerFlag);

				arrLuckyDrawResult.add(luckyDrawResult);

			}

		} catch (Exception e) {
			e.printStackTrace();

		} finally {
			try {
				if (rs != null)
					rs.close();
				if (stmt != null)
					stmt.close();
				if (conn != null)
					conn.close();

			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		return arrLuckyDrawResult;
	}

	private void searchMemberPhone() {
		String[] strMemberDBInfo = StartUpListener.arrMemberDBInfo.get(0);

		Connection connMemberDb = null;
		Statement stmtMemberDb = null;
		ResultSet rsMemberDb = null;

		try {
			// STEP 2: Register JDBC driver
			Class.forName(strMemberDBInfo[0]);

			// STEP 3: Open a connMemberDbection
			String strDBURL = strMemberDBInfo[1] + ";user=" + strMemberDBInfo[2] + ";password=" + strMemberDBInfo[3];

			connMemberDb = DriverManager.getConnection(strDBURL);

			System.out.println("********************** strDBURL : " + strDBURL);

			for (int j = 0; j < StartUpListener.LuckyDrawList.size(); j++) {
				String strUUID = "";

				strUUID = StartUpListener.LuckyDrawList.get(j).getMember_id().substring(0, 8) + "-" + StartUpListener.LuckyDrawList.get(j).getMember_id().substring(8, 12) + "-"
						+ StartUpListener.LuckyDrawList.get(j).getMember_id().substring(12, 16) + "-" + StartUpListener.LuckyDrawList.get(j).getMember_id().substring(16, 20) + "-"
						+ StartUpListener.LuckyDrawList.get(j).getMember_id().substring(20, 32);

				String strSQL = "SELECT [MSISDN] FROM [dbo].[TVBFunMemberLogin] where [AppMemberID] = '" + strUUID + "';";
				// System.out.println("**** ResultSet : " + strSQL + "\n");

				stmtMemberDb = connMemberDb.createStatement();
				rsMemberDb = stmtMemberDb.executeQuery(strSQL);

				while (rsMemberDb.next()) {
					StartUpListener.LuckyDrawList.get(j).setMobile_no(rsMemberDb.getString("MSISDN"));

				}
			}

		} catch (SQLException se) {
			// Handle errorsMemberDb for JDBC
			se.printStackTrace();
			System.out.println(se.getMessage());
		} catch (Exception e) {
			// Handle errorsMemberDb for Class.forName
			e.printStackTrace();
			System.out.println(StartUpListener.returnErrorMessage(e));
		} finally {
			try {
				if (rsMemberDb != null)
					rsMemberDb.close();
			} catch (SQLException se3) {
			}
			// finally block used to close resources
			try {
				if (stmtMemberDb != null)
					stmtMemberDb.close();
			} catch (SQLException se2) {
			} // nothing we can do
			try {
				if (connMemberDb != null)
					connMemberDb.close();
			} catch (SQLException se) {
				se.printStackTrace();
				System.out.println(StartUpListener.returnErrorMessage(se));
			} // end finally try
		}
		// return strPhone;

	}

}
