package tvb.vote.normal.resultdashboard;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import tvb.vote.normal.listener.StartUpListener;

@WebServlet("/LuckyDrawNewFormatServlet")
public class LuckyDrawNewFormatServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	@SuppressWarnings("null")
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.addHeader("Access-Control-Allow-Origin", "*");
		response.addHeader("Access-Control-Allow-Methods", "GET, PUT, POST, OPTIONS, DELETE");
		response.addHeader("Access-Control-Allow-Headers", "Content-Type");
		response.addHeader("Access-Control-Max-Age", "86400");

		String strProgramID = request.getParameter("program_ids");
		String strProgramName = request.getParameter("program_name");
		String strProgramTime = request.getParameter("program_time");

		SimpleDateFormat format = new SimpleDateFormat("dd/MMM/yyyy HHmm");
		String strCurrentDate = format.format(new Date());
		String strKey = "Lucky Draw - " + strProgramID + " - " + strCurrentDate;

		response.setHeader("Content-Type", "text/csv;charset=UTF-8");
		response.setHeader("Content-Disposition", "attachment;filename=luckydraw_" + strKey + ".csv");
		response.setCharacterEncoding("UTF-8");
		PrintWriter out = response.getWriter();

		try {
			// print excel
			out.write('\ufeff');
			out.print("\"Item\"," + "\"Programe ID\"," + "\"Programe Name\"," + "\"Programe Voting Time\","
					+ "\"Update Time\"," + "\"Member Id\"," + "\"Mobile No\"," + "\"Choice\"," + "\"Choice Id\","
					+ "\"Score\"," + "\"Item\"," + "\"Sign\"\r\n");

			for (int k = 0; k < StartUpListener.LuckyDrawList.size(); k++) {
				out.print("\"" + Integer.toString(k + 1) + "\"," + "\"" + strProgramID + "\"," + "\"" + strProgramName
						+ "\"," + "\"" + strProgramTime + "\"," + "\""
						+ StartUpListener.LuckyDrawList.get(k).getUpdate_time() + "\"," + "\""
						+ StartUpListener.LuckyDrawList.get(k).getMember_id() + "\"," + "\""
						+ StartUpListener.LuckyDrawList.get(k).getMobile_no() + "\"," + "\""
						+ StartUpListener.LuckyDrawList.get(k).getChoice() + "\"," + "\""
						+ StartUpListener.LuckyDrawList.get(k).getChoice_id() + "\"," + "\""
						+ StartUpListener.LuckyDrawList.get(k).getScore() + "\"," + "\"" + Integer.toString(k + 1)
						+ "\"\r\n");

				//System.out.println("\"" + StartUpListener.LuckyDrawList.get(k).getMember_id() + "\",\"" + StartUpListener.LuckyDrawList.get(k).getMobile_no() + "\",\"" + "1" + "\",\"" + StartUpListener.LuckyDrawList.get(k).getUpdate_time() + "\"\r\n");
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			out.flush();
		}

	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}