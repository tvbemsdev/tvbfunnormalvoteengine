package tvb.vote.normal.resultdashboard;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import tvb.vote.normal.listener.StartUpListener;

@WebServlet("/LuckyDrawOldFormatServlet")
public class LuckyDrawOldFormatServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	@SuppressWarnings("null")
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		response.addHeader("Access-Control-Allow-Origin", "*");
		response.addHeader("Access-Control-Allow-Methods", "GET, PUT, POST, OPTIONS, DELETE");
		response.addHeader("Access-Control-Allow-Headers", "Content-Type");
		response.addHeader("Access-Control-Max-Age", "86400");

		String strProgramID = request.getParameter("program_ids");

		SimpleDateFormat format = new SimpleDateFormat("dd/MMM/yyyy HHmm");
		String strCurrentDate = format.format(new Date());
		String strKey = "Lucky Draw- " + strProgramID + " - " + strCurrentDate;

		response.setHeader("Content-Type", "text/csv");
		response.setHeader("Content-Disposition", "attachment;filename= (Thomas Format)luckydraw_" + strKey + ".csv");
		PrintWriter out = response.getWriter();

		try {
			// print excel
			out.print("\"member_id\",\"phone\",\"score\",\"update_time\"\r\n");

			for (int k = 0; k < StartUpListener.LuckyDrawList.size(); k++) {
				out.print("\"" + StartUpListener.LuckyDrawList.get(k).getMember_id() + "\"," + "\""
						+ StartUpListener.LuckyDrawList.get(k).getMobile_no() + "\"," + "\"" + "1" + "\"," + "\""
						+ StartUpListener.LuckyDrawList.get(k).getUpdate_time() + "\"\r\n");

				//System.out.println("\"" + StartUpListener.LuckyDrawList.get(k).getMember_id() + "\",\"" + StartUpListener.LuckyDrawList.get(k).getMobile_no() + "\",\"" + "1" + "\",\"" + StartUpListener.LuckyDrawList.get(k).getUpdate_time() + "\"\r\n");
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			out.flush();
		}

	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}