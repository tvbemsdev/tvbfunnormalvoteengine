package tvb.vote.normal.resultdashboard;


import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import tvb.vote.normal.listener.StartUpListener;


@WebServlet("/ResultStatisticServlet")
public class ResultStatisticServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	private final String strDBDriver;
	private final String strDBURL;
	private final String strDBUser;
	private final String strDBPassword;
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ResultStatisticServlet() {
        super();
        // TODO Auto-generated constructor stub
        strDBDriver = StartUpListener.strDBDriver;
		strDBURL = StartUpListener.strDBURL;
		strDBUser = StartUpListener.strDBUser;
		strDBPassword = StartUpListener.strDBPassword;
        
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		
		response.addHeader("Access-Control-Allow-Origin", "*");
	    response.addHeader("Access-Control-Allow-Methods", "GET, PUT, POST, OPTIONS, DELETE");
	    response.addHeader("Access-Control-Allow-Headers", "Content-Type");
	    response.addHeader("Access-Control-Max-Age", "86400");
		
		String strResult = "false";
		String strData = "";
		
		String strProgramID = request.getParameter("program_ids");
		
		
    	Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		
		
		try {
			
			String strSQL = "SELECT option_ids, count(*) FROM tvb_fun_normal_vote where program_id='" + strProgramID + "' group by option_ids;";
			System.out.println("**** SQL : " + strSQL);
			
			Class.forName(strDBDriver);
			
			conn = DriverManager.getConnection(strDBURL, strDBUser, strDBPassword);
			stmt = conn.createStatement();
			
			rs = stmt.executeQuery(strSQL);
			
			strData = "{\"program_ids\": \"" + strProgramID + "\",\"choice\": [";
			
			while (rs.next()) {
				strData = strData + "{\"choice\": \"" + rs.getString("option_ids")  + "\",\"count\": " + rs.getString("count(*)") + "},";
				//System.out.println(rs.getString("option_ids") + " - " + rs.getString("count(*)"));
			}
			strResult = "true";
			
			if (strData != null && strData.length() > 0) {
				strData = strData.substring(0, strData.length()-1);
			    }
			
			strData = strData + "]}";
			
			rs.close();
			stmt.close();
			conn.close();
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (stmt != null)
					stmt.close();
				if (conn != null)
					conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		

		response.setContentType("text/x-json");
		PrintWriter out = response.getWriter();
		
		
		
		out.print("{");
		out.print("\"success\": " + strResult + ", \"data\": [" + strData + "]");
		out.print("}");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
