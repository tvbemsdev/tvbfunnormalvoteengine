package tvb.vote.normal.resultdashboard;

import java.io.IOException;
import java.io.PrintWriter;
import java.security.MessageDigest;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import tvb.vote.normal.listener.StartUpListener;

/**
 * Servlet implementation class CPSessionServlet
 */
@WebServlet("/CPSessionServlet")
public class CPSessionServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	private final int intMaxInactiveInterval;
	
	private final String strDBDriver;
	private final String strDBURL;
	private final String strDBUser;
	private final String strDBPassword;
	
	private final String strSessionTable;
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public CPSessionServlet() {
        super();
        // TODO Auto-generated constructor stub
        
        intMaxInactiveInterval = StartUpListener.intMaxInactiveInterval;
        
        strDBDriver = StartUpListener.strDBDriver;
		strDBURL = StartUpListener.strDBURL;
		strDBUser = StartUpListener.strDBUser;
		strDBPassword = StartUpListener.strDBPassword;
		
		strSessionTable = "tvb_voting_admin_account";
    }

	/**
	 * @see Servlet#init(ServletConfig)
	 */
	public void init(ServletConfig config) throws ServletException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
//		response.getWriter().append("Served at: ").append(request.getContextPath());
		
		response.addHeader("Access-Control-Allow-Origin", "*");
	    response.addHeader("Access-Control-Allow-Methods", "GET, PUT, POST, OPTIONS, DELETE");
	    response.addHeader("Access-Control-Allow-Headers", "Content-Type");
	    response.addHeader("Access-Control-Max-Age", "86400");
		
		boolean bolSuccess = false;
		
		String strAction   = request.getParameter("action");
		String strUserName = request.getParameter("username");
		String strPassword = request.getParameter("password");
		String strReason = "";
		String strResult = "";

		if (strAction == null){
			strAction = "";
		}
		
		if (strUserName == null){
			strUserName = "";
		}
		
		if (strPassword == null){
			strPassword = "";
		}
		
		System.out.println("********************************************************************\n"
				+ "**** CPSessionServlet\n"
				+ "**** Action   : " + strAction + "\n"
				+ "**** UserName : " + strUserName + "\n"
				+ "**** Password : " + strPassword + "\n"
				);
    	
		
		if (!strAction.equals("")){
			if (strAction.equals("login")){
				
				if (!strPassword.equals("") && !strUserName.equals("")){
					
					strPassword = CPSessionServlet.md5(strPassword);
					
					System.out.println("********************************************************************\n"
							+ "**** Password : " + strPassword + "\n"
							+ "**** Interval : " + intMaxInactiveInterval + "\n"
							);
							
			    	Connection conn = null;
					Statement stmt = null;
					ResultSet rs = null;
					
					boolean bolLogin = false;
					
					try {
						
						String strSQL = "SELECT * FROM " + strSessionTable + " where username='" + strUserName + "' and password='" + strPassword + "';";
						System.out.println("**** SQL : " + strSQL);
						
						Class.forName(strDBDriver);
						
						conn = DriverManager.getConnection(strDBURL, strDBUser, strDBPassword);
						stmt = conn.createStatement();
						
						rs = stmt.executeQuery(strSQL);
						while (rs.next()) {
							System.out.println(rs.getString("username") + " - " + rs.getString("password"));
							bolLogin = true;
						}
						
						rs.close();
						stmt.close();
						conn.close();
						
					} catch (Exception e) {
						e.printStackTrace();
					} finally {
						try {
							if (rs != null)
								rs.close();
							if (stmt != null)
								stmt.close();
							if (conn != null)
								conn.close();
						} catch (SQLException e) {
							e.printStackTrace();
						}
					}
					
					if (bolLogin){
						HttpSession session =  request.getSession();
						session.setAttribute("user", strUserName);
						
						session.setMaxInactiveInterval(intMaxInactiveInterval);
						bolSuccess = true;
						
					} else {
						strReason = "INVALID_USERNAME_OR_PASSWORD";
					}
					
				} else {
					strReason = "MISSING_USERNAME_OR_PASSWORD";
				}
				
			} else if (strAction.equals("logout")){
				
				 request.getSession().invalidate();
				 bolSuccess = true;
				 
			} else if (strAction.equals("check")){
			
				bolSuccess = StartUpListener.isLogined(request);
				if (!bolSuccess){
					strReason = "NOT_LOGIN";
				}
				
			} else if (strAction.equals("encrypt")){
				
				if (!strPassword.equals("")){
					
					strResult = CPSessionServlet.md5(strPassword);
					
				} else {
					strReason = "MISSING_PASSWORD";
				}
				
			} else {
				strReason = "INVALID_ACTION";
			}
		} else {
			strReason = "MISSING_ACTION_OR_USERNAME";
		}
	
		response.setContentType("text/x-json");
		PrintWriter out = response.getWriter();
		
		out.print("{");
		out.print("\"success\": " + Boolean.toString(bolSuccess));
		if (!strResult.equals("")){
			out.print(", \"result\": \"" + strResult + "\"");	
		}
		if (!strReason.equals("")){
			out.print(", \"reason\": \"" + strReason + "\"");
		}
		out.print("}");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}
	
	public static String md5(String str) {
		String md5 = null;
		try {
			MessageDigest md = MessageDigest.getInstance("MD5");
			byte[] barr = md.digest(str.getBytes());
			StringBuffer sb = new StringBuffer();
			for (int i = 0; i < barr.length; i++) {
				sb.append(byte2Hex(barr[i]));
			}
			String hex = sb.toString();
			md5 = hex.toUpperCase();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return md5;
	}

	public static String byte2Hex(byte b) {
		String[] h = { "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "a", "b", "c", "d", "e", "f" };
		int i = b;
		if (i < 0) {
			i += 256;
		}
		return h[i / 16] + h[i % 16];
	}
	  

}
