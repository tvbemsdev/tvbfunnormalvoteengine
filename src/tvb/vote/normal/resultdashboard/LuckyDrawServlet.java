package tvb.vote.normal.resultdashboard;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URLDecoder;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;

import tvb.vote.normal.listener.StartUpListener;
import tvb.vote.normal.model.LuckyDrawResult;
import tvb.vote.normal.service.LuckyDrawService;


@WebServlet("/LuckyDrawServlet")
public class LuckyDrawServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private LuckyDrawService luckyDrawService = new LuckyDrawService();
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	
		response.addHeader("Access-Control-Allow-Origin", "*");
	    response.addHeader("Access-Control-Allow-Methods", "GET, PUT, POST, OPTIONS, DELETE");
	    response.addHeader("Access-Control-Allow-Headers", "Content-Type");
	    response.addHeader("Access-Control-Max-Age", "86400");

	    StringBuilder sb = new StringBuilder();
	    BufferedReader reader = request.getReader();
	    try {
	        String line;
	        while ((line = reader.readLine()) != null) {
	            sb.append(line).append('\n');
	        }
	    } finally {
	        reader.close();
	    }

        String reqBody = URLDecoder.decode(sb.toString());  
        JSONObject json = new JSONObject(reqBody);  
        String strProgramID = json.getString("program_ids");
        String strOptionID = json.getString("option_ids");
        String strLuckyDrawNum = json.getString("luckydraw_num");
        String strQuizAns = json.getString("quiz_ans");
        String strMapping = json.getString("mapping");

	    PrintWriter out = response.getWriter();
	    String jsonResult = "";

		// Generate Lucky Draw List
		try {
			
			StartUpListener.LuckyDrawList = new ArrayList<LuckyDrawResult>();
			luckyDrawService.doLuckyDraw(strProgramID, strOptionID, strLuckyDrawNum, strMapping, strQuizAns);
			jsonResult = "{\"resultcode\":200}";
			
		} catch (Exception e) {
			e.printStackTrace();
			jsonResult = "{\"resultcode\":500}";
		} finally {
	        response.setContentType("application/json");
	        response.setCharacterEncoding("UTF-8");
	        
	        out.print(jsonResult);	
			out.flush();
			
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
