package tvb.vote.normal.thread;

import java.io.PrintWriter;

import java.io.StringWriter;
import java.io.Writer;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.ConcurrentHashMap;

import tvb.vote.normal.listener.StartUpListener;
import tvb.vote.normal.model.MemberInfo;


public class UpdateMemberThread extends Thread {

	private boolean isRunning = false;
	
	private ArrayList<String[]> arrMemberDBInfo;
	private ConcurrentHashMap<String, MemberInfo> mapMemberInfo;
	
	private Date dateCPLastUpdateMember;
	private int intInterval = 5 * 60 * 1000;
	
	private SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
	
	public UpdateMemberThread(){

		arrMemberDBInfo = StartUpListener.arrMemberDBInfo;
		mapMemberInfo = StartUpListener.mapMemberInfo;
		
		dateCPLastUpdateMember = new java.util.Date(0);
		
//		dateUpdateTime = null;
		
		System.out.println("\nINIT : " + UpdateMemberThread.class.getName());
	}
	
	public boolean isThreadRunning(){
		return isRunning;
	}
	
	public void run() {
		
		SimpleDateFormat sdfKey = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss.SSS");
        String strThreadKey = "CM_" + sdfKey.format(new java.util.Date());
        
		//System.out.println("Collect Member Thread - START");
        System.out.println("\n********************************************************************\n**** CP Collect Member Thread - START");
        StringBuffer strLog = null;
        
        isRunning = true;
        try {
			while (true){
				
				strLog = new StringBuffer("\n");
				strLog.append("********************************************************************\n");
				strLog.append("**** UpdateMemberThread - [KEY:" + strThreadKey + "] RUNNING\n");
	        	strLog.append("**** NOW : " + new java.util.Date() + " \n");
	        	strLog.append("**** --------------------------\n");
	        	strLog.append("**** SIZE : " + arrMemberDBInfo.size() + "\n");
	        	
	        	System.out.println(strLog);
				
				boolean bolUpdate = true;
//				boolean bolUpdate = false;
//				if (dateUpdateTime == null){
//					bolUpdate = true;
//				} else {
//					
//					SimpleDateFormat sdf = new SimpleDateFormat("yyyy-mm-dd");
//					
//					Calendar calNext = Calendar.getInstance();
//					calNext.setTime(sdf.parse(sdf.format(dateUpdateTime)));
//					calNext.add(Calendar.HOUR, intUpdateAfterHour);
//					
//					Calendar calNow = Calendar.getInstance();
//					
//					if (calNow.after(calNext)){
//						bolUpdate = true;
//					}
//				}
				
				if (bolUpdate){
					
					//System.out.println("CPCollectMemberInfoThread - Size : " + arrMemberDBInfo.size());
//					strLog = new StringBuffer("\n");
//					strLog.append("********************************************************************\n");
//					
//					System.out.println(strLog);
					
					boolean bolUpdateSuccess = true;
					
					for (int i=0; i<arrMemberDBInfo.size(); i++){
						
						String[] strMemberDBInfo = arrMemberDBInfo.get(i);
						
						strLog = new StringBuffer("\n");
						
						Connection conn = null;
						Statement stmt = null;
						
						try {
							// STEP 2: Register JDBC driver
							Class.forName(strMemberDBInfo[0]);
			
							// STEP 3: Open a connection
//							System.out.println("Connecting to database...");
							strLog.append("********************************************************************\n");
							strLog.append("**** DataBase [" + (i+1) + "] Connecting to database...\n");
							
							String strDBURL = strMemberDBInfo[1] + ";user=" + strMemberDBInfo[2] + ";password=" + strMemberDBInfo[3];
//							System.out.println(strDBURL);
							strLog.append("**** URL : " + strDBURL + "\n");
							
							conn = DriverManager.getConnection(strDBURL);
							stmt = conn.createStatement();
							
							String strSQL = "SELECT * FROM dbo.TVBFunMemberLogin where";
							if (dateCPLastUpdateMember == null){
								strSQL += " AccountStatus = 1";
							} else {
								strSQL += " LastUpdatedDate > \'" + sdf.format(dateCPLastUpdateMember) + "\'";
							}
							strLog.append("**** SQL : " + strSQL + "\n");
							ResultSet rs = stmt.executeQuery(strSQL);
							
							int intAddCount = 0;
							int intRemoveCount = 0;
							while (rs.next()) {
								
								//String[] strMemberInfo = new String[5];
//								if (rs.getString("AppMemberID") != null) strMemberInfo[0] = rs.getString("AppMemberID").trim().toUpperCase();
//								if (rs.getString("CountryCode") != null) strMemberInfo[1] = rs.getString("CountryCode").trim();
//								if (rs.getString("MSISDN") != null) strMemberInfo[2] = rs.getString("MSISDN").trim();
//								if (rs.getString("AccountStatus") != null) strMemberInfo[3] = rs.getString("AccountStatus").trim();
//								if (rs.getString("NewLogonSession") != null) strMemberInfo[4] = rs.getString("NewLogonSession").trim();
//								mapMemberInfo.put(strMemberInfo[0], strMemberInfo);
//								System.out.println(strMemberInfo[0] + " - " + strMemberInfo.toString());
								
								
								if (rs.getString("AppMemberID") != null){
									
									String strMemberId = rs.getString("AppMemberID").replace("-", "").trim().toUpperCase();
									
									int intStatus = -1;
									if (rs.getString("AccountStatus") != null) {
										try {
											intStatus = Integer.parseInt(rs.getString("AccountStatus").trim());
										} catch (Exception e){
											intStatus = -1;
										}
										
									}
									
									if (intStatus != -1 && intStatus > 0){
										
										MemberInfo infoMember = new MemberInfo();
										
										infoMember.setAccountStatus(intStatus);
										infoMember.setMemberId(strMemberId);
									
										if (rs.getString("CountryCode") != null) infoMember.setCountryCode(rs.getString("CountryCode").trim());
										if (rs.getString("MSISDN") != null) infoMember.setMsisdn(rs.getString("MSISDN").trim());
										if (rs.getString("NewLogonSession") != null) infoMember.setLogonSession(rs.getString("NewLogonSession").trim());
										
										
										mapMemberInfo.put(strMemberId, infoMember);
										intAddCount++;
									} else {
										
										if (mapMemberInfo.get(strMemberId) != null){
											mapMemberInfo.remove(strMemberId);
											intRemoveCount++;
										}
									}
								}
							}
							
							//System.out.println("done");
							strLog.append("**** " + Integer.toString(intAddCount) + " (Updated / Added) & " + Integer.toString(intRemoveCount) + " (Removed)\n");
							System.out.println(strLog);
							
							rs.close();
							stmt.close();
							conn.close();
							
						} catch (SQLException se) {
							// Handle errors for JDBC
							se.printStackTrace();
							System.out.println(strLog);
							System.out.println(se.getMessage());
							
							bolUpdateSuccess = false;
						} catch (Exception e) {
							// Handle errors for Class.forName
							e.printStackTrace();
							System.out.println(strLog);
							System.out.println(this.returnErrorMessage(e));
							
							bolUpdateSuccess = false;
						} finally {
							// finally block used to close resources
							try {
								if (stmt != null)
									stmt.close();
							} catch (SQLException se2) {
							} // nothing we can do
							try {
								if (conn != null)
									conn.close();
							} catch (SQLException se) {
								se.printStackTrace();
								System.out.println(strLog);
								System.out.println(this.returnErrorMessage(se));
							} // end finally try
						}
					}
					
					strLog = new StringBuffer("\n");
					strLog.append("********************************************************************\n");
					
					if (bolUpdateSuccess){
						Calendar calNow = Calendar.getInstance();
						calNow.add(Calendar.HOUR, -6);
						
						dateCPLastUpdateMember.setTime(calNow.getTimeInMillis());
						
						strLog.append("**** Last Update Time : " + dateCPLastUpdateMember.toString() + " (" + Long.toString(dateCPLastUpdateMember.getTime()) + ")\n");
					} else {
						strLog.append("**** Update All Member NOT Complete\n");
					}
					
					System.out.println(strLog);
					
					//dateUpdateTime = new Date();
				}
				
				strLog = new StringBuffer("\n");
				strLog.append("********************************************************************\n");
				
				System.out.println(strLog);
				
				try {
		        	Thread.sleep(intInterval);
		        } catch (Exception e){
		        	Thread.currentThread().interrupt();
	        		break;
		        }
			}
			
		} catch (Exception e){
			e.printStackTrace();
		}
		
        isRunning = false;
    }
	
	private String returnErrorMessage(Exception e){
		Writer wr = new StringWriter();
    	PrintWriter pwr = new PrintWriter(wr);
    	e.printStackTrace(pwr);
    	return wr.toString();
	}
}
