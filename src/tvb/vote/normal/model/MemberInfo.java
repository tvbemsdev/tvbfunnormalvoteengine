package tvb.vote.normal.model;

import org.json.JSONObject;

public class MemberInfo {
	
	private String memberId = "";
	private String countryCode = "";
	private String msisdn = "";
	private String logonSession = "";
	private int accountStatus = -1;
	
	public String getMemberId() {
		return memberId;
	}

	public void setMemberId(String memberId) {
		this.memberId = memberId;
	}

	public String getCountryCode() {
		return countryCode;
	}
	
	public void setCountryCode(String contryCode){
		this.countryCode = contryCode;
	}
	
	public String getMsisdn() {
		return msisdn;
	}
	
	public void setMsisdn(String msisdn){
		this.msisdn = msisdn;
	}
	
	public String getLogonSession() {
		return logonSession;
	}
	
	public void setLogonSession(String logonSession){
		this.logonSession = logonSession;
	}
	
	public int getAccountStatus() {
		return accountStatus;
	}
	
	public void setAccountStatus(int accountStatus){
		this.accountStatus = accountStatus;
	}

	public String toString(){
	
		return "{\"member_id\":\"" + memberId + "\","
				+ "\"contry_code\":\"" + countryCode + "\","
				+ "\"msisdn\":\"" + msisdn + "\","
				+ "\"logon_session\":\"" + logonSession + "\","
				+ "\"account_status\":" + Integer.toString(accountStatus) + "}";
	}
	
	public JSONObject getJsonObject(){
		
		JSONObject objReturn = new JSONObject();
		objReturn.put("member_id", memberId);
		objReturn.put("country_code", countryCode);
		objReturn.put("msisdn", msisdn);
		objReturn.put("logon_session", logonSession);
		objReturn.put("account_status", accountStatus);
		return objReturn;
	}
}